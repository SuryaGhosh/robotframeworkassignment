*** Settings ***
Library  SeleniumLibrary
Resource  ../PageObjects/ItemSearchPageObjects.robot

*** Keywords ***

I search for ${itemToSearch} in search box
    Wait Until Element Is Visible       ${SearchTextField_locator}      60s
    Click Element  ${SearchTextField_locator}
    Clear Element Text  ${SearchTextField_locator}
    Input Text  ${SearchTextField_locator}  ${itemToSearch}

    Click Element  ${SearchButton_locator}


