*** Settings ***
Library  SeleniumLibrary
Resource  ../Config.robot

*** Keywords ***
Launch Test URL
    Open Browser   ${URL}  ${BROWSER}
    Maximize browser window
    Set Browser Implicit Wait  10

Close Browser
    Close All Browsers