*** Settings ***
Library  SeleniumLibrary
Resource  ../PageObjects/HomePageObjects.robot

*** Keywords ***

I am at Amazon India website
    Wait Until Element Is Visible       ${AmazonIndiaLogo_locator}      60s


I am viewing the ${pageName} page
    Wait Until Element Is Visible       ${SearchTextField_locator}      60s


I click on ${categoryToSelect} option in category
    @{categoryList}=  Get WebElements  ${TopMenuItems_locator}
    FOR     ${category}     IN      @{categoryList}
        ${categoryName}=  Get Text  ${category} 
        IF      '${categoryName}' == '${categoryToSelect}'
            Click Element  ${category}
            Exit For Loop
        END
    END


I hover on ${categoryToSelect}
    Wait Until Element Is Visible       ${CategoryLabel_locator}      60s
    Log  ${categoryToSelect}
    @{categoryList}=  Get WebElements  ${CategoryList_locator}
    FOR     ${category}     IN      @{categoryList}
        ${categoryName}=  Get Text  ${category} 
        IF      '${categoryName}' == '${categoryToSelect}'
            # Scroll Element Into View  ${category}
            # Click Element  ${category}
            Mouse Over    ${category}
            Exit For Loop
        END
    END


I click on ${itemToSelect}
    Wait Until Element Is Visible       ${FlyoutItemList_locator}      60s
    Log  ${itemToSelect}
    @{itemList}=  Get WebElements  ${FlyoutItemList_locator}
    FOR     ${item}     IN      @{itemList}
        ${itemName}=  Get Text  ${item} 
        IF      '${itemName}' == '${itemToSelect}'
            Click Element  ${item}
            Exit For Loop
        END
    END


I click on Prime checkbox
    Wait Until Element Is Visible       ${Prime_locator}      60s
    Scroll Element Into View  ${Prime_locator}
    Click Element  ${Prime_locator}
