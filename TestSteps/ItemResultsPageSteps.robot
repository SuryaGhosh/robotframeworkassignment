*** Settings ***
Library  SeleniumLibrary
Resource  ../PageObjects/ItemResultsPageObjects.robot

*** Keywords ***

I validate search result appeared for ${itemName}
    Wait Until Element Is Visible       ${SearchResultLabel_locator}      60s
    Wait Until Keyword Succeeds    5x    5s    Condition Should Be Met  ${itemName}  



Condition Should Be Met
    [Arguments]  ${item}
    ${actualResultText}=  Get Text      ${SearchResultLabel_locator} 
    Should Contain      ${actualResultText}      ${item}


I select sort by dropdown as ${dropdownValue}
    Wait Until Element Is Visible       ${SortByFilter_locator}      60s
    Click Element  ${SortByFilter_locator}

    Wait Until Element Is Visible       ${SortByDropdownList_locator}      60s
    @{dropdownList}=  Get WebElements  ${SortByDropdownList_locator}
    FOR     ${ddValue}     IN      @{dropdownList}
        ${dropdownName}=  Get Text  ${ddValue} 
        IF      '${dropdownName}' == '${dropdownValue}'
            Click Element  ${ddValue}
            Exit For Loop
        END
    END


I verify first product name and price
    Wait Until Element Is Visible       ${ItemName_locator}      60s
    ${firstItemName}=  Get Text  ${ItemName_locator}  
    ${firstItemPrice}=  Get Text  ${ItemPrice_locator} 
    Log  "First Item Name"
    Log  ${firstItemName} 
    Log  "First Item Price" 
    Log  ${firstItemPrice}


I click on the product
    Click Element  ${ItemName_locator}

