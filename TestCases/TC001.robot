*** Settings ***
Resource  ../TestSteps/CommonSteps.robot
Resource  ../TestSteps/HomePageSteps.robot
Resource  ../TestSteps/ItemSearchPageSteps.robot
Resource  ../TestSteps/ItemResultsPageSteps.robot
Resource  ../TestSteps/ItemDetailsPageSteps.robot
Resource  ../TestSteps/LoginPageSteps.robot



Test SetUp  Launch Test URL
Test Teardown  Close Browser


*** Test Cases ***
Amazon Scenario

    Given I am at Amazon India website 
    Then I am viewing the Home page
    Then I click on Mobiles option in category
    Then I hover on Mobiles & Accessories
    Then I click on Headsets
    Then I click on Prime checkbox
    Then I search for Headphones in search box
    Then I validate search result appeared for Headphones
    Then I select sort by dropdown as Price: Low to High
    Then I verify first product name and price
    Then I click on the product
    And I click on the Buy Now button
    Then I verify page user is redirected to Log in