*** Variables ***

${SearchResultLabel_locator}  //span[@data-component-type='s-result-info-bar']//span[contains(@class,'a-text-bold')]
${SortByFilter_locator}  //label[text()='Sort by:']/parent::span
${SortByDropdownList_locator}  //div[@class='a-popover-inner']//li
${ItemName_locator}  //div[contains(@class,'puis-card-container')]//div[contains(@class,'title')]
${ItemPrice_locator}  //div[contains(@class,'puis-card-container')]//span[@class='a-price-whole']