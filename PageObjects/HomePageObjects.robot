*** Variables ***

${AmazonIndiaLogo_locator}  //div[@id='nav-logo']/a[@aria-label='Amazon.in']
${SearchTextField_locator}  //input[@id='twotabsearchtextbox']
${TopMenuItems_locator}  //div[@class='nav-progressive-content']/a[contains(@class,'nav-a')]
${CategoryLabel_locator}  //span[text()='Category']
${CategoryList_locator}  //div[@id='nav-subnav']//span[@class='nav-a-content']
${FlyoutItemList_locator}  //div[@class='mega-menu']//li/a
${Prime_locator}  //i[@aria-label='Prime Eligible']
${CategoryCount_locator}  //div[@id='departments']//div[@id='n-title']